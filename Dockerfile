FROM golang:1.17rc2-alpine3.14 as base

WORKDIR /app
COPY . .

RUN apk update && apk add --no-cache make
RUN apk add build-base
RUN go mod tidy
RUN go mod download

FROM base AS build
RUN make test && make build

FROM alpine:3.14 AS bin
WORKDIR /app
RUN mkdir ./config
COPY --from=build /app/bin/app ./app
COPY --from=build /app/bin/console ./console
COPY --from=build /app/config/app.yml ./config

ENTRYPOINT ["sh", "-c", "./app"]

CMD []
