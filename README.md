# AroundHomeAssignment

## How to run & test

The project uses a mongodb database.
It lives within a docker container and can be started using 
docker-compose command or using make: 

```
make start
```

The command will run 3 containers: project, database and swagger ui

After the first execution the database needs to be seeded with some demo data. It can be done with a separate console command:

```
make seedDbDocker
```

It will connect with the database and generate a list of partners.
The script will generate a few fake partners with coordinates within Wildberg, Germany region.
The demo below shows what the service returns when the coordinates provided are also within Wildberg.

The single endpoint <b>/v1/getPartners</b> uses POST method (posts json payload as a request)
and returns 5 records (limits the requested records to keep the response small).
The limit can be changed in the internal/app/partnerRepository.go, limitOutput constant.

### Other commands

Makefile offers several commands to run parts of the project, execute certain scripts.
For example:

```
make run - run outside of docker using go run
make test - run tests
```

### Swagger & example

Swagger ui is available at http://localhost:18081/

Example using curl:

```
curl -X 'POST' \
  'http://localhost:8080/v1/getPartners' \
  -H 'accept: application/json' \
  -H 'Content-Type: application/json' \
  -d '{
    "Material": "WOOD",
    "Address": {
        "Longitude": 8.735741487000181,
        "Latitude":  48.63309018722691
    },
    "SquareMeters": 20.8,
    "PhoneNumber": "234324324234"
}'
```
Response:
```
{
    "Partners": [
        {
            "Id": "6330bc897c538c8da94e6a01",
            "ExperiencedWithMaterials": [
                "TILES",
                "CARPET",
                "WOOD"
            ],
            "Address": {
                "Latitude": 48.62282245726562,
                "Longitude": 8.741057170750587
            },
            "OperatingRadius": 6000,
            "Rating": 5,
            "Distance": 1208.0537
        },
        {
            "Id": "6330bc897c538c8da94e69ef",
            "ExperiencedWithMaterials": [
                "WOOD"
            ],
            "Address": {
                "Latitude": 48.62410703438572,
                "Longitude": 8.73574148700018
            },
            "OperatingRadius": 1000,
            "Rating": 4.5,
            "Distance": 999.9942
        },
        {
            "Id": "6330bc897c538c8da94e69f4",
            "ExperiencedWithMaterials": [
                "TILES",
                "WOOD"
            ],
            "Address": {
                "Latitude": 48.62084584784869,
                "Longitude": 8.729826450773421
            },
            "OperatingRadius": 2300,
            "Rating": 3.8,
            "Distance": 1430.8208
        },
        {
            "Id": "6330bc897c538c8da94e69f1",
            "ExperiencedWithMaterials": [
                "WOOD",
                "CARPET"
            ],
            "Address": {
                "Latitude": 48.63176551059099,
                "Longitude": 8.746856561137077
            },
            "OperatingRadius": 3000,
            "Rating": 3.4,
            "Distance": 830.9167
        },
        {
            "Id": "6330bc897c538c8da94e6a02",
            "ExperiencedWithMaterials": [
                "TILES",
                "CARPET",
                "WOOD"
            ],
            "Address": {
                "Latitude": 48.644901534720184,
                "Longitude": 8.769827139630651
            },
            "OperatingRadius": 3900,
            "Rating": 3.4,
            "Distance": 2831.1584
        }
    ]
}
```

## Dev guide

The project uses schema driven appoach.
Schema is described in a proto file /api/app.proto.
The command below generates go modules in pkg/app/generated:

```
make generate
```

Service provides grpc and rest endpoints (ports are defined in cmd/app/main.go).
One test was added as an example. It can be executed with:

```
make test
```

Service provides metrics (prometheus), liveness endpoints on a separate port (in docker 18080:8080).

#### How does it work?

Mongodb fits well in this context due to the nature of the data.
It also supports geospatial queries out of the box, which is essential for the task.
Initially, I considered a memory storage, haversine formula to determine distances.
In this case I would have to load all the data in a collection and process it in memory filtering and sorting using 
the functionality available in go. This approach wouldn't work for a large amount of data, though.
Therefore, I decided to use something closer to a real life environment.

There is no solution in Mongodb, which would allow searching for a set of circles (address and operating radius), intersecting with particular coordinates.
I had to find another way. I created an additional property (polygon) and wrote a simple app to generate polygons out of circles.

```
go run ./cmd/console/main.go --setpolygons
```

These values allowed me to use $geoIntersects functionality (supports polygons).

The endpoint accepts requests from the customer, passes the values to the repository (where it is passed to mongodb using aggregate with filtering).
The response is converted to the desired format and sent back as an output.

The file config/app.yml has a connection to the docker database and must be updated if another database/environment is being used.