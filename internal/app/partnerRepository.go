package aroundhomeassignment

import (
	"context"
	"fmt"

	api "gitlab.com/ruslancer/AroundHomeAssignment/pkg/app/generated"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

const limitOutput = 5

type PartnerRepository struct {
	mongoCollection *mongo.Collection
}

func (p *PartnerRepository) GetPartners(ctx context.Context, address api.Coordinates, material api.Material) ([]*api.Partner, error) {
	items := make([]*api.Partner, 0)

	materialId := api.Material_name[int32(material)]
	pipeline := bson.A{
		bson.D{{
			"$geoNear", bson.D{
				{
					"near", bson.D{
						{"type", "Point"},
						{"coordinates", bson.A{address.Longitude, address.Latitude}},
					},
				},
				{
					"distanceField", "distance",
				},
				{
					"query", bson.D{
						{
							"experiencedWithMaterials", bson.D{{
								"$all", bson.A{materialId},
							}},
						},
						{
							"polygon", bson.D{{
								"$geoIntersects", bson.D{{"$geometry", bson.D{
									{"type", "Point"}, {"coordinates", bson.A{address.Longitude, address.Latitude}},
								}}},
							}},
						},
					},
				},
			},
		}},
		bson.D{{
			"$sort", bson.D{{"rating", -1}, {"distance", 1}},
		}},
		bson.D{{"$limit", limitOutput}},
	}

	cur, err := p.mongoCollection.Aggregate(ctx, pipeline)
	if err != nil {
		return items, err
	}

	for cur.Next(ctx) {
		partner := &Partner{}
		if err := cur.Decode(partner); err != nil {
			return items, err
		}

		items = append(items, partner.toProtobufPartner())
	}

	return items, nil
}

func (p *PartnerRepository) TruncatePartners(ctx context.Context) error {
	_, err := p.mongoCollection.DeleteMany(ctx, bson.D{})
	if err != nil {
		return err
	}

	return nil
}

func (p *PartnerRepository) AddIndex(ctx context.Context) error {
	indexModel := mongo.IndexModel{
		Keys: bson.D{{"address", "2dsphere"}},
	}

	_, err := p.mongoCollection.Indexes().CreateOne(ctx, indexModel)

	fmt.Println("add index", err)

	return err
}

func (p *PartnerRepository) StorePartners(ctx context.Context, items []Partner) error {
	for _, partner := range items {
		_, err := p.mongoCollection.InsertOne(ctx, partner)
		if err != nil {
			return err
		}
	}

	return nil
}

func (p *PartnerRepository) SetPolygonsInObjects(ctx context.Context) error {
	cur, err := p.mongoCollection.Find(ctx, bson.D{})
	if err != nil {
		return err
	}
	for cur.Next(ctx) {
		partner := &Partner{}
		err := cur.Decode(partner)
		if err != nil {
			return err
		}

		id, _ := primitive.ObjectIDFromHex(partner.GetId())
		coordinates := circleToPolyCoords(partner.Address.Coordinates[0], partner.Address.Coordinates[1], partner.OperatingRadius, 32)

		_, err = p.mongoCollection.UpdateOne(ctx, bson.D{{"_id", id}}, bson.D{{"$set", bson.D{{"polygon", Polygon{Coordinates: [][][2]float64{coordinates}, Type: "Polygon"}}}}})
		if err != nil {
			fmt.Println("error occured")
			return err
		}

		fmt.Printf("Update object %s\n", id)
	}

	return nil
}

func NewPartnerRepository(collection *mongo.Collection) *PartnerRepository {
	return &PartnerRepository{
		mongoCollection: collection,
	}
}
