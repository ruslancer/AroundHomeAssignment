package aroundhomeassignment

type DbConfig struct {
	Connection string
	Collection string
	DbName     string
}

type Config struct {
	Db DbConfig
}
