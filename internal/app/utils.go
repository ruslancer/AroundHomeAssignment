package aroundhomeassignment

import (
	"context"
	"time"

	"github.com/heptiolabs/healthcheck"
	"github.com/spf13/viper"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
)

func InitHealthChecks() healthcheck.Handler {
	health := healthcheck.NewHandler()
	health.AddLivenessCheck("goroutine-threshold", healthcheck.GoroutineCountCheck(100))
	health.AddReadinessCheck("upstream-dep-dns", healthcheck.DNSResolveCheck("localhost", 50*time.Millisecond))

	return health
}

func GetDbConnection(ctx context.Context, config Config) (*mongo.Client, error) {
	dbClient, err := mongo.Connect(ctx, options.Client().ApplyURI(config.Db.Connection))
	if err != nil {
		return nil, err
	}

	// Ping the primary
	if err = dbClient.Ping(context.TODO(), readpref.Primary()); err != nil {
		return nil, err
	}

	return dbClient, nil
}

func LoadConfig(path string) (config Config, err error) {
	viper.AddConfigPath(path)
	viper.SetConfigName("app")
	viper.SetConfigType("yml")
	viper.AutomaticEnv()

	err = viper.ReadInConfig()
	if err != nil {
		return
	}

	config.Db = DbConfig{
		Connection: viper.GetString("db.mongodb.connection"),
		Collection: viper.GetString("db.mongodb.collection"),
		DbName:     viper.GetString("db.mongodb.dbname"),
	}

	return
}
