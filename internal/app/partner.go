package aroundhomeassignment

import api "gitlab.com/ruslancer/AroundHomeAssignment/pkg/app/generated"

type Point struct {
	Coordinates []float64 `bson:"coordinates"`
	Type        string    `bson:"type"`
}
type Polygon struct {
	Coordinates [][][2]float64 `bson:"coordinates"`
	Type        string         `bson:"type"`
}

type Partner struct {
	Id                       string   `bson:"_id,omitempty"`
	ExperiencedWithMaterials []string `bson:"experiencedWithMaterials,omitempty"`
	Address                  Point    `bson:"address,omitempty"`
	OperatingRadius          float64  `bson:"operatingRadius,omitempty"`
	Rating                   float64  `bson:"rating,omitempty"`
	Polygon                  Polygon  `bson:"polygon,omitemtpy"`
	Distance                 float64  `bson:"distance,omitempty"`
}

func (p *Partner) GetId() string {
	return p.Id
}

func (p *Partner) toProtobufPartner() *api.Partner {
	materials := []api.Material{}
	for _, material := range p.ExperiencedWithMaterials {
		materials = append(materials, api.Material(api.Material_value[material]))
	}

	return &api.Partner{
		Id:                       p.GetId(),
		Rating:                   float32(p.Rating),
		OperatingRadius:          float32(p.OperatingRadius),
		ExperiencedWithMaterials: materials,
		Address:                  &api.Coordinates{Latitude: p.Address.Coordinates[1], Longitude: p.Address.Coordinates[0]},
		Distance:                 float32(p.Distance),
	}
}
