package aroundhomeassignment

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_CircleToPolyCoords(t *testing.T) {
	testCases := []struct {
		lat    float64
		lon    float64
		radius float64
		points int
	}{
		{
			lat:    8.73574148700018,
			lon:    48.62410703438572,
			radius: 1000.0,
			points: 32,
		},
		{
			lat:    8.743745198515764,
			lon:    48.625794835458066,
			radius: 2000.0,
			points: 32,
		},
	}
	for _, tc := range testCases {
		t.Run(fmt.Sprintf("%f, %f", tc.lon, tc.lat), func(t *testing.T) {
			result := circleToPolyCoords(tc.lon, tc.lat, tc.radius, tc.points)

			assert.Equal(t, 33, len(result))
		})
	}
}
