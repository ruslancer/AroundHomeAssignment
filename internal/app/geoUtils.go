package aroundhomeassignment

import "math"

func toRadians(angleInDegrees float64) float64 {
	return (angleInDegrees * math.Pi) / 180
}

func toDegrees(angleInRadians float64) float64 {
	return (angleInRadians * 180) / math.Pi
}

func offset(ln, lt, distance, earthRadius, bearing float64) [2]float64 {
	lat1 := toRadians(lt)
	lon1 := toRadians(ln)
	dByR := distance / earthRadius
	lat := math.Asin(
		math.Sin(lat1)*math.Cos(dByR) + math.Cos(lat1)*math.Sin(dByR)*math.Cos(bearing),
	)

	var lon = lon1 +
		math.Atan2(
			math.Sin(bearing)*math.Sin(dByR)*math.Cos(lat1),
			math.Cos(dByR)-math.Sin(lat1)*math.Sin(lat),
		)
	return [2]float64{toDegrees(lon), toDegrees(lat)}
}

func circleToPolyCoords(lon, lat, radius float64, npoints int) [][2]float64 {
	var earthRadius float64 = 6378137
	coordinates := [][2]float64{}
	var direction float64 = 1

	start := toRadians(0)
	var i int
	for i = 0; i < npoints; i++ {
		coords := offset(
			lon, lat, radius, earthRadius, start+(direction*2*float64(math.Pi)*-float64(i))/float64(npoints),
		)

		coordinates = append(coordinates, coords)
	}

	coordinates = append(coordinates, coordinates[0])

	return coordinates
}
