package aroundhomeassignment

import (
	"context"
	"fmt"

	api "gitlab.com/ruslancer/AroundHomeAssignment/pkg/app/generated"
)

type PartnersRepositoryInterface interface {
	GetPartners(ctx context.Context, address api.Coordinates, material api.Material) ([]*api.Partner, error)
}

type PartnerServer struct {
	api.UnimplementedPartnersServiceServer
	Repository PartnersRepositoryInterface
}

func (p *PartnerServer) GetPartners(ctx context.Context, request *api.GetPartnersRequest) (*api.GetPartnersResponse, error) {
	items, err := p.Repository.GetPartners(ctx, *request.Address, request.Material)
	if err != nil {
		fmt.Println(err)

		return &api.GetPartnersResponse{
			Partners: []*api.Partner{},
		}, err
	}

	return &api.GetPartnersResponse{
		Partners: items,
	}, nil
}
