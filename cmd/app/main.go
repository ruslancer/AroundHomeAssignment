package main

import (
	"context"
	"fmt"
	"log"
	"net"
	"net/http"

	app "gitlab.com/ruslancer/AroundHomeAssignment/internal/app"

	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/rs/cors"

	api "gitlab.com/ruslancer/AroundHomeAssignment/pkg/app/generated"

	grpcPrometheus "github.com/grpc-ecosystem/go-grpc-prometheus"
	"github.com/grpc-ecosystem/grpc-gateway/v2/runtime"
	"github.com/tmc/grpc-websocket-proxy/wsproxy"
	"golang.org/x/sync/errgroup"
	"google.golang.org/grpc"
)

const grpcPort = "50051"
const systemPort = "8080"
const httpPort = "80"

func main() {
	ctx := context.Background()
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()

	config, err := app.LoadConfig("./config")
	if err != nil {
		log.Fatalf("failed to load config %s", err.Error())
	}

	healthCheckHandler := app.InitHealthChecks()

	netListener, err := net.Listen("tcp", ":"+grpcPort)
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}

	grpcServer := grpc.NewServer(
		grpc.StreamInterceptor(grpcPrometheus.StreamServerInterceptor),
		grpc.UnaryInterceptor(grpcPrometheus.UnaryServerInterceptor),
	)

	mongoClient, err := app.GetDbConnection(ctx, config)
	if err != nil {
		log.Fatalf("failed to connect to mongodb: %v", err)
	}
	defer func() {
		if err = mongoClient.Disconnect(ctx); err != nil {
			panic(err)
		}
	}()

	api.RegisterPartnersServiceServer(grpcServer, &app.PartnerServer{
		Repository: app.NewPartnerRepository(
			mongoClient.Database(config.Db.DbName).Collection("partners"),
		),
	})

	grpcPrometheus.Register(grpcServer)
	var group errgroup.Group

	group.Go(func() error {
		err = grpcServer.Serve(netListener)

		if err != nil {
			log.Fatalf("failed to serve: %v", err)
		}

		return err
	})

	serveMux := runtime.NewServeMux(runtime.WithMarshalerOption(runtime.MIMEWildcard, &runtime.JSONPb{}))
	opts := []grpc.DialOption{
		grpc.WithInsecure(),
		grpc.WithDefaultCallOptions(grpc.MaxCallRecvMsgSize(500)),
	}

	group.Go(func() error {
		r := http.NewServeMux()
		r.Handle("/", healthCheckHandler)
		r.Handle("/metrics", promhttp.Handler())

		//serve metrics
		return http.ListenAndServe(":"+systemPort, r)
	})

	group.Go(func() error {
		return api.RegisterPartnersServiceHandlerFromEndpoint(ctx, serveMux, ":"+grpcPort, opts)
	})

	group.Go(func() error {
		//serve http
		return http.ListenAndServe(":"+httpPort, cors.AllowAll().Handler(wsproxy.WebsocketProxy(serveMux)))
	})

	if err := group.Wait(); err != nil {
		fmt.Println(err)
	}
}
