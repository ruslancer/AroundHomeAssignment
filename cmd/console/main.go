package main

import (
	"context"
	"flag"
	"fmt"
	"log"

	app "gitlab.com/ruslancer/AroundHomeAssignment/internal/app"
)

var setpolygons = flag.Bool("setpolygons", false, "updatepolygons")
var seedDb = flag.Bool("seedDb", false, "seed db")

func main() {
	flag.Parse()

	ctx := context.Background()
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()

	config, err := app.LoadConfig("./config")
	if err != nil {
		log.Fatalf("failed to load config %s", err.Error())
	}

	mongoClient, err := app.GetDbConnection(ctx, config)
	if err != nil {
		log.Fatalf("failed to connect to mongodb: %v", err)
	}
	defer func() {
		if err = mongoClient.Disconnect(ctx); err != nil {
			panic(err)
		}
	}()

	repository := app.NewPartnerRepository(
		mongoClient.Database(config.Db.DbName).Collection("partners"),
	)

	if *setpolygons {
		err = repository.SetPolygonsInObjects(ctx)
		fmt.Println(err)
		return
	}

	if *seedDb {
		items := []app.Partner{
			app.Partner{
				ExperiencedWithMaterials: []string{"WOOD"},
				Rating:                   4.5,
				OperatingRadius:          1000,
				Address: app.Point{
					Type:        "Point",
					Coordinates: []float64{8.73574148700018, 48.62410703438572},
				},
			},
			app.Partner{
				ExperiencedWithMaterials: []string{"CARPET"},
				Rating:                   5,
				OperatingRadius:          2000,
				Address: app.Point{
					Type:        "Point",
					Coordinates: []float64{8.743745198515764, 48.625794835458066},
				},
			},
			app.Partner{
				ExperiencedWithMaterials: []string{"WOOD", "CARPET"},
				Rating:                   3.4,
				OperatingRadius:          3000,
				Address: app.Point{
					Type:        "Point",
					Coordinates: []float64{8.746856561137077, 48.63176551059099},
				},
			},
			app.Partner{
				ExperiencedWithMaterials: []string{"CARPET"},
				Rating:                   2.8,
				OperatingRadius:          1200,
				Address: app.Point{
					Type:        "Point",
					Coordinates: []float64{8.75728498972454, 48.63360904426416},
				},
			},
			app.Partner{
				ExperiencedWithMaterials: []string{"TILES"},
				Rating:                   4.8,
				OperatingRadius:          6000,
				Address: app.Point{
					Type:        "Point",
					Coordinates: []float64{8.754331111846325, 48.61583852180639},
				},
			},
			app.Partner{
				ExperiencedWithMaterials: []string{"TILES", "WOOD"},
				Rating:                   3.8,
				OperatingRadius:          2300,
				Address: app.Point{
					Type:        "Point",
					Coordinates: []float64{8.729826450773421, 48.62084584784869},
				},
			},
			app.Partner{
				ExperiencedWithMaterials: []string{"WOOD"},
				Rating:                   4.7,
				OperatingRadius:          500,
				Address: app.Point{
					Type:        "Point",
					Coordinates: []float64{8.728989601580103, 48.6271858567254},
				},
			},
			app.Partner{
				ExperiencedWithMaterials: []string{"CARPET"},
				Rating:                   3.3,
				OperatingRadius:          3100,
				Address: app.Point{
					Type:        "Point",
					Coordinates: []float64{8.716222286639484, 48.623711006442726},
				},
			},
			app.Partner{
				ExperiencedWithMaterials: []string{"TILES"},
				Rating:                   1.5,
				OperatingRadius:          4500,
				Address: app.Point{
					Type:        "Point",
					Coordinates: []float64{8.706158638360105, 48.62749787239719},
				},
			},
			app.Partner{
				ExperiencedWithMaterials: []string{"TILES", "CARPET", "WOOD"},
				Rating:                   5.0,
				OperatingRadius:          1400,
				Address: app.Point{
					Type:        "Point",
					Coordinates: []float64{8.699206352691629, 48.60884445610227},
				},
			},
			app.Partner{
				ExperiencedWithMaterials: []string{"TILES"},
				Rating:                   4.9,
				OperatingRadius:          4800,
				Address: app.Point{
					Type:        "Point",
					Coordinates: []float64{8.714334011267514, 48.61989548699607},
				},
			},
			app.Partner{
				ExperiencedWithMaterials: []string{"WOOD"},
				Rating:                   2.3,
				OperatingRadius:          1000,
				Address: app.Point{
					Type:        "Point",
					Coordinates: []float64{8.722166061477015, 48.62442017901739},
				},
			},
			app.Partner{
				ExperiencedWithMaterials: []string{"WOOD"},
				Rating:                   1.8,
				OperatingRadius:          1900,
				Address: app.Point{
					Type:        "Point",
					Coordinates: []float64{8.742572307663805, 48.628306267667895},
				},
			},
			app.Partner{
				ExperiencedWithMaterials: []string{"CARPET"},
				Rating:                   4.0,
				OperatingRadius:          5000,
				Address: app.Point{
					Type:        "Point",
					Coordinates: []float64{8.732551575128097, 48.63356761170596},
				},
			},
			app.Partner{
				ExperiencedWithMaterials: []string{"TILES", "WOOD"},
				Rating:                   4.9,
				OperatingRadius:          1100,
				Address: app.Point{
					Type:        "Point",
					Coordinates: []float64{8.777942908971246, 48.619900509831865},
				},
			},
			app.Partner{
				ExperiencedWithMaterials: []string{"TILES", "CARPET", "WOOD"},
				Rating:                   5.0,
				OperatingRadius:          4000,
				Address: app.Point{
					Type:        "Point",
					Coordinates: []float64{8.781891120438967, 48.61391424450462},
				},
			},
			app.Partner{
				ExperiencedWithMaterials: []string{"CARPET"},
				Rating:                   2.2,
				OperatingRadius:          800,
				Address: app.Point{
					Type:        "Point",
					Coordinates: []float64{8.77111936928394, 48.62025512401761},
				},
			},
			app.Partner{
				ExperiencedWithMaterials: []string{"TILES", "WOOD"},
				Rating:                   4.2,
				OperatingRadius:          1000,
				Address: app.Point{
					Type:        "Point",
					Coordinates: []float64{8.748202575686687, 48.62424081738742},
				},
			},
			app.Partner{
				ExperiencedWithMaterials: []string{"TILES", "CARPET", "WOOD"},
				Rating:                   5.0,
				OperatingRadius:          6000,
				Address: app.Point{
					Type:        "Point",
					Coordinates: []float64{8.741057170750587, 48.62282245726562},
				},
			},
			app.Partner{
				ExperiencedWithMaterials: []string{"TILES", "CARPET", "WOOD"},
				Rating:                   3.4,
				OperatingRadius:          3900,
				Address: app.Point{
					Type:        "Point",
					Coordinates: []float64{8.769827139630651, 48.644901534720184},
				},
			},
		}

		_ = repository.TruncatePartners(ctx)
		err = repository.StorePartners(ctx, items)

		_ = repository.AddIndex(ctx)

		fmt.Println(err)
		return
	}

	fmt.Println("unknown command")
}
