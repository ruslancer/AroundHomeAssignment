grpc-generate:
	rm -rf /tmp/grpc-gateway
	rm -rf /tmp/protobuf
	git clone --depth 1 --branch v2.1.0 http://github.com/grpc-ecosystem/grpc-gateway /tmp/grpc-gateway
	git clone --depth 1 https://github.com/protocolbuffers/protobuf.git /tmp/protobuf
	cp -R /tmp/grpc-gateway/third_party/googleapis/google ./api/google
	cp -R /tmp/protobuf/src/google/* ./api/google
	cp -R /tmp/grpc-gateway/protoc-gen-openapiv2 ./api/protoc-gen-openapiv2
	rm -rf /tmp/grpc-gateway
	rm -rf /tmp/protobuf
	mkdir -p ./pkg/app

	protoc -I ./api \
		--go_out=./pkg/app \
		--openapiv2_out=. \
    	--openapiv2_opt logtostderr=true \
		--gohttp_out=./pkg/app \
		--grpc-gateway_out=./pkg/app \
		--go-grpc_out=./pkg/app ./api/app.proto

	rm -rf ./api/protoc-gen-openapiv2

.PHONY: generate
generate:
	go generate ./...

.PHONY: build
build:
	go build -v -o ./bin/app ./cmd/app
	go build -v -o ./bin/console ./cmd/console

.PHONY: run
run:
	go run ./cmd/app/main.go

.PHONY: test
test:
	$(info #Running tests...)
	go test --count=1 -cover ./...

.PHONY: swagger
swagger:
	docker-compose up -d swagger	

.PHONY: start
start:
	docker-compose up -d

.PHONY: startdb
startdb:
	docker-compose up -d mongodb

.PHONY: updatePolygons
updatePolygons:
	go run ./cmd/console/main.go --setpolygons

.PHONY: seedDb
seedDb:
	go run ./cmd/console/main.go --seedDb
	go run ./cmd/console/main.go --setpolygons

.PHONY: seedDbDocker 
seedDbDocker:
	docker exec partners-service sh -c "./console --seedDb && ./console --setpolygons"
