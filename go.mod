module gitlab.com/ruslancer/AroundHomeAssignment

go 1.14

replace gitlab.com/ruslancer/AroundHomeAssignment/pkg => ./pkg

replace gitlab.com/ruslancer/AroundHomeAssignment/internal => ./internal

require (
	github.com/grpc-ecosystem/go-grpc-prometheus v1.2.0
	github.com/grpc-ecosystem/grpc-gateway/v2 v2.11.3
	github.com/heptiolabs/healthcheck v0.0.0-20211123025425-613501dd5deb
	github.com/nametake/protoc-gen-gohttp v1.6.0
	github.com/prometheus/client_golang v1.13.0
	github.com/rs/cors v1.8.2
	github.com/spf13/viper v1.13.0
	github.com/tmc/grpc-websocket-proxy v0.0.0-20220101234140-673ab2c3ae75
	go.mongodb.org/mongo-driver v1.10.2
	golang.org/x/sync v0.0.0-20220601150217-0de741cfad7f
	google.golang.org/genproto v0.0.0-20220923205249-dd2d53f1fffc
	google.golang.org/grpc v1.49.0
	google.golang.org/grpc/cmd/protoc-gen-go-grpc v1.1.0
	google.golang.org/protobuf v1.28.1
	gopkg.in/DATA-DOG/go-sqlmock.v1 v1.3.0 // indirect
)
